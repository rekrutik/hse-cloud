fastapi==0.115.9
uvicorn==0.34.0
prometheus-fastapi-instrumentator==7.0.2
