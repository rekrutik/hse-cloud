from fastapi import FastAPI, HTTPException

from prometheus_fastapi_instrumentator import Instrumentator
import logging

app = FastAPI()

logger = logging.getLogger("uvicorn.error")

instrumentator = Instrumentator().instrument(app).expose(app)


@app.get("/")
def read_root():
    return {"message": "Hello World"}


@app.get("/trigger-500")
def trigger_500():
    logger.error(
        "THERE IS IMPORTANT INFO TO FIX THIS ERROR: ALL YOU NEED IS A SIMPLE SOVIET..."
    )
    raise HTTPException(status_code=500, detail="Internal Server Error")
