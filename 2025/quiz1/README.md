# Листок 1

Почитать/посмотреть:

- https://www.bcs.org/articles-opinion-and-research/history-of-the-cloud/
- Davis C. Cloud Native Patterns: Designing Change-tolerant Software. – Manning Publications, 2019., параграф 1.2: Introducing cloud native software, много картинок, можно почитать по диагонали
- Облачные вычисления – лекция от Олега Сухорослова от 20.01.2022 https://youtu.be/XNcOLUuuY3s
- https://habr.com/ru/company/yandex/blog/432042/

Extras для любопытных:

- https://www2.eecs.berkeley.edu/Pubs/TechRpts/2009/EECS-2009-28.pdf
- https://habr.com/ru/company/yandex/blog/487694/

Ответить на вопросы нужно текстом и сдать ссылку на GitHub Gist/hackmd.io или загрузить PDF:

1. Сравните стоимость виртуальной машины или bare metal-хоста аналогичных спецификаций в любом облачном провайдере (AWS, GCP, Яндекс.Облако) и традиционном серверном хостинге (например, Hetzner или OVH). Какое решение будет дешевле, если работу инстанса нужно поддерживать на постоянной основе?
2. Приведите два любых примера, в одном из которых использование простой виртуалки будет удобнее/эффективнее, чем Docker-контейнера, и второй пример наоборот — когда удобнее контейнеры, чем просто виртуалки.
3. В Яндекс.Облаке существуют [прерываемые ВМ](https://yandex.cloud/ru/docs/compute/concepts/preemptible-vm), которые значительно дешевле обычных. Почему так? В каких кейсах их можно применить?

*Дисклеймер про ответы на вопросы: я не жду каких-то конкретных ответов на вопросы, не буду ставить разные оценки если кто-то нашел недостаточно старую инфу или дал ответ, отличный от моего мнения по теме. Оценка очень простая — 0, 0.5, 1, 1.5 за каждый вопрос:*

- *0 будет только если совсем ничего содержательного не написали/скипнули вопрос,*
- *0.5 за ненулевой ответ,*
- *1 — дефолтная оценка, норм ответ,*
- *1.5 — если очень подробно и круто*

*Итоговая будет считаться не исходя из 1.5 за все листки, а исходя из какого-то разумного нормирования.*
