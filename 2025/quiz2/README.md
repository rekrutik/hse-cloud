# Листок 2

Посмотреть:

- [Лекция Олега Сухорослова про хранилища](https://youtu.be/8tnvl0yhFrc)

## Вопросы (до 1.5 баллов за каждый)

1. Придумайте и опишите задачу и архитектуру, в которой имело бы смысл использовать Network File System вместо более продвинутой сетевой файловой системы (S3, HDFS, GlusterFS и подобных).
2. Придумайте и опишите задачу и архитектуру, в которой было бы удобно использовать как Network File System, так и object storage.

В обоих случаях выше опишите необходимость и архитектуру такого решения так, как будто ваши коллеги говорят “да всё на диск положим и норм”, а вас это решение не устраивает и вам нужно убедить их в необходимости более комплексного решения.

## Задачки (до 1.5 баллов за правильное решение каждой)

### 1. Прайсинг Object Storage

Допустим, вы делаете какой-то около-офисный продукт. Ваши пользователи открывают документ, совершают в нем какие-то операции, закрывают. Операции пишутся в историю в какой-то полностью бесплатный черный ящик, который сможет вам эту историю отдать, никакого отношения к объектному хранилищу этот черный ящик не имеет. С периодичностью раз в день ваша виртуальная машина в той же зоне доступности, что и хранилище, скачивает документ из объектного хранилища, историю из черного ящика, проверяет появилось ли что-то новое в истории относительно скаченного документа, применяет новые операции к документу и кладет документ обратно в объектное хранилище. Предположим, один документ занимает в объектном хранилище в среднем 10 МБ и особо не растет. У вас 10000 пользователей, у них в среднем по 10 документов и открывают они их раз в неделю (всегда делают какие-то изменения). Каждое открытие — это полное скачивание документа из объектного хранилища. Допустим, что новых документов не появляется.

[Диаграмма описываемого сервиса](https://viewer.diagrams.net/?border=0&tags=%7B%7D&highlight=FFFFFF&edit=_blank&layers=1&nav=1&title=week3-task3.drawio&open=R5Vlbd9o4EP41PDbHV3AeE0i3D%2B3ZbNm2m74JW2ClwvKRZS799TuyZYwsG0hwSE7KA0hjaSTNN%2FNpxgzc8XLzF0dp%2FIVFmA4cK9oM3MnAcXwvgG8p2CpBYJWCBSdRKbJrwZT8xkpYDctJhDNtoGCMCpLqwpAlCQ6FJkOcs7U%2BbM6ovmqKFtgQTENETekPEom4lAa%2BVcs%2FYbKIq5VtSz1ZomqwEmQxith6T%2BTeDdwxZ0yUreVmjKm0XWWXct7Hjqe7jXGciFMmfJn%2BEzrej5%2F33uLh%2B7%2Ffvn%2F9Nrn7UJk5E9vqxDgCA6gu4yJmC5YgeldLb8Ocr7DUakOHszyJip4FvXrCZ8ZSNeQRC7FV0KJcMBDFYknVU9g%2B3%2F4n51%2F5Vfdh%2F9lko5SXva3qzVkixowyXmzbtYoPyMsDyVN0GkqJMpbzEB%2ByjnI4xBdYHBjn7OCEMMBsiWGfMI9jigRZ6ftAyiEXu3E1ZtBQsD0FQqV4hWiulgI%2FSyhDEUhjkgmmVmmi%2FBnNIFg1MBAliwTaIdgKg2VvV5gLAuFwox4sSRQVTsBxRn6jWaFPmj1lJBHF2fzbgT85CpBUjDdtMayU1pGzD90BLzYBUOqtKysIrktdioYqaE%2BGSCm%2Fl6fcG8Lm8wxco4nhbg9nwGqg%2BqmC0prCr%2BStJqh6LK5jIvA0RYWLr4GddajnhNI2eBqwfSw%2Bh2AzIqsTiKGlgeCNVH9dM6tdgR7vsWo1r%2F%2FY8d4S%2FVmjff6zrrwjDFj07jEnYAwZrAdpEW%2BIKJdxfNV9qPRAu15CdqoVeuRS50Qudc%2Fk0mLqDedouzdAkZMZx8oxr3XHtK%2BdhmuVCvsNcN%2BI8DxVrB2xMF9KEztDCsa6nXFoLWRrTSBS4GBpSgmWQxO8%2FmNo3jtG89cjt1ear9xDU%2BqM9Pkvdwc4hod8xYucIvAG65HN4JslpousCBc5okUOGsYkOfOe6J%2F3Xee1ed9%2FBu2fzfRdgaLdJ13ZsHXsLtjRezDS%2BP3KP8jwstO8QnpkffdE1u%2BI7Mtk0CMjzFiKE42G3zOt%2BkdY1Q9cr8Gqw7eePQdvKa8bPSmSL1DXnhqVr1rXXhtRuVfWZglKs5i988gMDkbmB0h4HD0ynV7SnV1ZpLQGl8p3XAPyv2eP8q1ed8kLWkmaSeCyGKVSGFKWR08ufyOEg3ko1QjOfuG9J8MwwLN5P4mQ7TYKjWFLJuS2ZELBi1XAztthSlujyTr%2FeT2i9E4kyg7UL%2FQC0AyblGViVxtCMsNhG7JUeM9kaZ97W108SfEM3MZQ1BeLT7oyz2xNlhQl%2BASC67B8g%2Fj6fL%2B3K%2ByqQs9uobehb9Kb7b0Yvw0NIxs2ffZr04QVQEQoi3d0aARPH3Zt3BquaVbHa7k1PPfFzGrWTFO0hOvZulkhQtGMUCLkCX6ylvcPYAmhW1W%2FdZVdW0x9OjW1wagDrcHWGS0gry5D%2BzJQ2m1QPgNJ6Nb%2FLpacVf9F6979Dw%3D%3D)

Изучите прайсинг объектного хранилища в Яндекс.Облаке и Amazon Web Services и посчитайте, во сколько вам будет обходиться объектное хранилище в каждом из облаков.

### 2. SLA вашего сервиса

Найдите в интернете Average Failure Rate или SLA для Amazon Elastic Block Storage с точки зрения

1. времени доступности самого диска
2. надежности данных == *как часто они их могут просто потерять?*

Помимо этого, найдите SLA для виртуальных машин (EC2).

Предположим, в вашем кластере тысяча серверов и у каждого из них по два подключенных диска, ваш сервер держит эти диски синхронизированными и читает если хотя бы один жив (RAID 1). Какой SLA вы можете предоставить внешним пользователям, если предположить, что ваше приложение работает без ошибок и падения могут случаться только в случае отключения EC2 или смерти/недоступности обоих дисков?

---

*Дисклеймер про ответы на вопросы: я не жду каких-то конкретных ответов на вопросы, не буду ставить разные оценки если кто-то нашел недостаточно старую инфу или дал ответ, отличный от моего мнения по теме. Оценка очень простая — 0, 0.5, 1, 1.5 за каждый вопрос:*

- *0 будет только если совсем ничего содержательного не написали/скипнули вопрос,*
- *0.5 за ненулевой ответ,*
- *1 — дефолтная оценка, норм ответ,*
- *1.5 — если очень подробно и круто*

*Итоговая будет считаться не исходя из 1.5 за все листки, а исходя из какого-то разумного нормирования.*